"
I simply display grid data on a canvas of BlElements.

Sample Program to test colors:

"">>>""
grid := Array2D rows: 1 columns: 50.
counter := 0.
1 to: (grid numberOfRows) do: [ :y |
	1 to: (grid numberOfColumns) do: [ :x |
		counter := counter + 1.
		grid at: y at: x put: counter
	]
].

HeatmapDisplay fromGridData: grid.
""<<<""
"
Class {
	#name : #HeatmapDisplay,
	#superclass : #Object,
	#category : #Heatmap
}

{ #category : #display }
HeatmapDisplay class >> fromGridData: grid [
	"I answer a BlElement with heatmap data from grid (Array2D)"

	| frame blockSize width height posX posY child max|
	blockSize := 10.
	width := grid numberOfColumns.
	height := grid numberOfRows.
	max := grid max.
	frame := BlElement new size: (width * blockSize)@(height * blockSize); background: (Color white).

	1 to: width do: [ :x | 
		1 to: height do: [ :y |
			posX := (x-1) * blockSize.
			posY := (y-1) * blockSize.
			child := HeatmapCell new
				size: blockSize@blockSize;
				relocate: posX@posY;
				opacity: 1;
				background: (self rgbFromValue: (grid at: y at: x) maxAt: max).
			frame addChild: child. 
		].
	 ].

	^ frame
]

{ #category : #converting }
HeatmapDisplay class >> grayFromValue: aNumber [
	"I convert number into a color.
	(greyscale for now)"

	| safeValue intensity |
	safeValue := aNumber min: 255 max: 1.
	intensity := 255 / safeValue.
	^ Color gray: intensity
]

{ #category : #converting }
HeatmapDisplay class >> rgbFromValue: aNumber maxAt: maxNumber [
	"I convert number into a color (and normalize the max of values)."

	| norm red green blue |
	
	aNumber = 0 
	ifTrue: [ 
		red := 0.
		green := 0.
		blue := 0.
	]
	ifFalse: [ 
		norm := aNumber / maxNumber.
		red   := norm >= 0.5 ifTrue: 1 ifFalse: 1/2 + norm * 2.
		green := (1/2 + (((norm * 3) * 2) cos / -2)) min: 0.95.
		blue  := norm <= 0.5 ifTrue: 1 ifFalse: 1/2 - norm * 2.
	].
	
^ Color fromRgbTriplet: (Array with: red with: green with: blue)
]

{ #category : #generating }
HeatmapDisplay class >> sampleRandom [
	"I just generate a BlElement with random colors"

	| frame posX posY child |
	frame := BlElement new size: 100@100; background: (Color red).

	0 to: 9 do: [ :x | 
		0 to: 9 do: [ :y |
			posX := x * 10.
			posY := y * 10.
			child := HeatmapCell new
				relocate: posX@posY;
				background: (Color random).
			frame addChild: child. 
		].
	 ].

	^ frame
]
