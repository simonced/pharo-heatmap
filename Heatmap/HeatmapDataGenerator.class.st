"
Data generator for heat map.

Sample usage: HeatmapDataGenerator randomClicks: 200 width: 10 height: 10.
"
Class {
	#name : #HeatmapDataGenerator,
	#superclass : #Object,
	#category : #Heatmap
}

{ #category : #generation }
HeatmapDataGenerator class >> randomClicks: numClicks width: numX height: numY [
	"I simulate user clicks at random in a canvas of specified size.
	I return a an Array2D with number of clicks at each location.
	
	Sample: HHeatmapDataGenerator randomClicks: 200 width: 10 height: 10."

	| grid posX posY rnd|
	rnd := Random new.
	grid := Array2D rows: numX columns: numY element: 0.
	1 to: numClicks do: [ :i |
		posX := rnd nextInt: numX.
		posY := rnd nextInt: numY.
		grid at: posX at: posY incrementBy: 1.
	].

	^ grid
]
