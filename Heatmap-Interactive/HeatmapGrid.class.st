"
I am a grid with interactive cells.
Cells can receive clicks and I handle the recoloring.

""sample >>>""
HeatmapGrid emptyWithGridSize: 20@20 andBlockSize: 10@10.
""<<<""
"
Class {
	#name : #HeatmapGrid,
	#superclass : #BlElement,
	#instVars : [
		'grid'
	],
	#category : #'Heatmap-Interactive'
}

{ #category : #'instance creation' }
HeatmapGrid class >> emptyWithGridSize: gSize [
	"I generate the canvas with interractive elements.
	gSize is a Point defining how many blocks I handle in X and Y.
	default cell size is used (HeatmapCell defaultDimensions)."

^ self emptyWithGridSize: gSize andBlockSize: (HeatmapCell defaultDimensions)
]

{ #category : #'instance creation' }
HeatmapGrid class >> emptyWithGridSize: gSize andBlockSize: bSize [
	"I generate the canvas with interractive elements.
	gSize is a Point defining how many blocks I handle in X and Y.
	bSize is a Point defining each cell size in x and y."

	| frame child pos |
	
	frame := self new
		size: bSize * gSize; 
		background: (Color white).
	
	frame grid: (Array2D rows: (gSize y) columns: (gSize x)).

	1 to: gSize x do: [ :x | 
		1 to: gSize y do: [ :y |
			pos := bSize * ((x-1)@(y-1)).

			"init child"
			child := HeatmapCell new
				size: bSize;
				relocate: pos;
				background: (Color white);
				parentGrid: frame;
				posGrid: x@y.
			frame addChild: child. 
			
			"keep instance at same coordinates"
			frame grid at: y at: x put: child.
		].
	 ].

	^ frame
]

{ #category : #accessing }
HeatmapGrid >> fetchNeighboorsAround: aCell [
	"I retreive the neighbooring cells around aCell"

	| neighboors min_x min_y max_x max_y |
	
	"future results"
	neighboors := OrderedCollection new.
	
	"better way to check boundaries?"
	min_x := (aCell posGrid x - 1) max: 1.
	min_y := (aCell posGrid y - 1) max: 1.
	max_x := (aCell posGrid x + 1) min: (self grid numberOfColumns).
	max_y := (aCell posGrid y + 1) min: (self grid numberOfRows).
	
	min_x to: max_x do: [ :x |
		min_y to: max_y do: [ :y |
			neighboors add: (self grid at: y at: x).
		].
	].

^ neighboors 
]

{ #category : #accessing }
HeatmapGrid >> grid [
	^ grid
]

{ #category : #accessing }
HeatmapGrid >> grid: anObject [
	grid := anObject
]

{ #category : #initialization }
HeatmapGrid >> initialize [
	super initialize.	
	^ self
]

{ #category : #drawing }
HeatmapGrid >> repaint [
	"I regenerate the colors of the cell (my children)"

	| maxClicks |
	maxClicks := 0.
	children do: [ :child |
		maxClicks := maxClicks max: (child clicks).
	].
	"TODO move the maxClicks to a separate method"
	
	children do: [ :child |
		child clicks = 0 ifTrue: [ child opacity: 0 ] ifFalse: [ child opacity: 1 ].
		child background: (HeatmapDisplay rgbFromValue: (child clicks) maxAt: maxClicks).
	].
]

{ #category : #updating }
HeatmapGrid >> updateClicksAround: aCell [
	"'naturally' spreading clicks to neighboors
	The idea is to 'share' some of the clicks to neighboors"

	| clicksToShare neighboors |
	clicksToShare := aCell clicks * (1/3) floor.
	clicksToShare  > 0 ifTrue: [
		neighboors := self fetchNeighboorsAround: aCell.
		neighboors do:	[ :cell |
			cell addClicks: clicksToShare.
		]
	].

	"then we repaint"
	self repaint	
]
