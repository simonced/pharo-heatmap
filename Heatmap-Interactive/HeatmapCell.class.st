"
I just represent a small screen area that counts how many clicks I received.
The grid itself (my parent/container) will then change my color accordingly.
"
Class {
	#name : #HeatmapCell,
	#superclass : #BlElement,
	#instVars : [
		'clicks',
		'parentGrid',
		'posGrid'
	],
	#category : #'Heatmap-Interactive'
}

{ #category : #initialization }
HeatmapCell class >> defaultDimensions [
	"default size of a cell"

	^ 10@10
]

{ #category : #adding }
HeatmapCell >> addClicks: aNumber [
	"I add clicks to the cell"

	clicks := clicks + aNumber.
]

{ #category : #accessing }
HeatmapCell >> clicks [
	^ clicks
]

{ #category : #initialization }
HeatmapCell >> initialize [
	"I just initiate a block with a click count of 0"

	super initialize.

	"Fixed size for now"
	self size: (self class defaultDimensions).
	
	"Empty background"
	self background: Color white.
	
	"reset clicks count to 0"
	clicks := 0.
	
	self addEventFilterOn: BlMouseDownEvent do: [ 
		self addClicks: 1.
		"the line below is not working, for some reason..."
		self parentGrid updateClicksAround: self.
	].
	
^ self
]

{ #category : #accessing }
HeatmapCell >> parentGrid [
	^ parentGrid
]

{ #category : #accessing }
HeatmapCell >> parentGrid: anObject [
	parentGrid := anObject
]

{ #category : #accessing }
HeatmapCell >> posGrid [
	^ posGrid
]

{ #category : #accessing }
HeatmapCell >> posGrid: anObject [
	posGrid := anObject
]
